set INPUTDIR=%CD%/source
set OUTPUTDIR=%CD%/output
set STYLEDIR=%CD%/style

set BIBFILE=%CD%/references.bib

pandoc "%INPUTDIR%"/*.md -o "%OUTPUTDIR%/thesis.pdf" -H "%STYLEDIR%/preamble.tex" --template="%STYLEDIR%/template.tex" --bibliography="%BIBFILE%" 2>pandoc.log --csl="%STYLEDIR%/ref_format.csl" --highlight-style pygments -V fontsize=12pt -V papersize=a4paper -V documentclass:report -N --latex-engine=xelatex 