---
title:  'Pflichtenheft'
date: 02.03.2017
publisher: ViMu GmbH
author:
- Tim Schmidt
- Mai Tran
- Lion Scherer
- Paul Bauriegel
- Marius Jochheim
tags: [vimu, dhbw, videovertonung]

papersize: a4
link-citations: true
toc: true
bibliography: true
...

#Problemstellung
Skateboarding ist Ausdruck eines individuellen Lebensgefühls. Dazu gehört das Einüben und insbesondere das Zeigen und in Szene setzen von Skateboard-Tricks. Die Tricks werden in Videos festgehalten und mit der Community geteilt. Manche Videos sind kurz und sollen einfach und schnell geteilt werden.

Die bisherige Software zur Bearbeitung und Vertonung von Videos ist teuer und kompliziert in der Bedienung. Der Anwender muss sich außerdem viele Gedanken bei der musikalischen Untermalung machen und lizenz-rechtliche Fragen bedenken, wenn er das Video auf Youtube hochladen und teilen will. Wenn der Anwender nur kurze Szenen oder wenige Tricks zeigen möchte, ist die Verwendung einer solchen Software viel zu aufwendig.

Der Titus-Skateshop möchte seine Kundschaft bei der Video-Erstellung unterstützen und benötigt etwas, das er seinen Premium-Kunden zusätzlich anbieten kann und das die Marke stärkt.

#Ziele
Ziel ist es, Titus eine Software bereitzustellen, die Skateboarding-Videos automatisch vertont, so dass einfacher und mehr Videos hochgeladen und geteilt werden können. Die Video-Vertonungs-Software wird von Titus seiner Premium-Kundschaft kostenfrei zur Verfügung gestellt.

Da die Kunden von Titus in den Videos häufig die Skateboards und Kleidung von Titus tragen, wird die Marke durch mehr Videos gestärkt. Der Marken-Name wird zusätzlich in der Software erkennbar sein.

Um die Ziele zu erreichen, ist bei der Video-Vertonung zu beachten, dass die Musik zum Video passt, die Vertonung einzigartig und die Musik lizenzfrei ist. Außerdem ist die Benutzung der Software einfach. Das vertonte Video kann gespeichert und geteilt werden.

#Umfeldanalyse
##Konkurrenzprodukte

Natürlich gibt es auch andere Möglichkeiten Videos zu vertonen:
- Man kann sein Video selbst mit Musik hinterlegen, was aber sehr zeitaufwendig ist und oftmals Probleme mit der GEMA mit sich bringt. Außerdem sind die Leute oftmals unmusikalisch und wollen sich keine Gedanken darüber machen mit welcher Musik sie ihr Video am besten hinterlegen können.
- Um ein Video mit Musik oder Liedern zu hinterlegen wird eine Software benötigt und wenn diese einigermaßen professionell sein soll, wird Zeit und technisches Know-How benötigt um sie zu bedienen. Die Anwender unserer Software wollen schnell und einfach ihre Videos mit passender Musik hinterlegen und sich nicht stundenlang um passende Effekte kümmern. Unsere Software unterlegt Videos automatisch mit passenden Effekten und das sogar anhand von Merkmalen des Videos (Bewegung, Farben, etc).
- Außerdem bekommen unsere Anwender, die Premium Kunden von Titus, die Software kostenlos zur Verfügung gestellt, was ihnen einen Kostenvorteil bringt im Vergleich zu anderen Softwareangeboten.

#Stakeholderanalyse
##Interessensgruppen
Lieferant
ViMu

##Kunde
Titus
Skateshop: Skateboards, Longboards, Skateschuhe, Streetwear
cool, dynamisch
stellt seinen Premium-Kd die ViMu-SW zur Verfügung > Kundenbindung + Marketing

##Anwender
Typischer Kunde von Titus, die ViMu nutzen sollen:

- Skater, die schnell und unkompliziert Videos vertonen und teilen wollen (Youtube, Facebook, Vimeo)
- Premium-Mitglieder
- Videolänge (1-7min)
- Video-Equipment, Laptop, Musik
- Alter: 13-25 Jahre
- wollen ihre Tricks zeigen, ihren Style, ihre Skateboards
- Videos: viel Bewegung, Action, Schnelligkeit, cool
- erhalten die Software: müssen sich nicht mit GEMA etc. beschäftigen, autom. Vertonung anhand von Bewegungen, neues Tool, das sie ausprobieren können, Geld- und ZeitersparnisWer sieht sich die Videos an?

- andere Skater, die auch Videos produzieren und hochladen > gefallen die Skateboards, die Vertonung (Nachfrage, wie das erstellt wurde)
- Freundes-/Bekannten-Kreis/Follower/andereSportler/andere Kunden für Titus
- sehen die Videos und interessieren sich dann für Titus, deren Produkte (und die Video-Vertonung)#

#Use Case


#Funktionen
##Funktional

   * **F1 Vertonung des Videos**
   * Der Nutzer der Anwendung nutzt die Anwendung um Musik zu seinem Video zu generieren. Diese ist ästetisch ansprechende und somit an das zugrundeliegende Video auf verschiedene Arten angepasst.
       * **F1-1 Video-Analyse**
       * Um die Musik passend an ein Video zu generien müssen zuerst Aspekte des Videos analysiert werden. Diese werden in den einzelnen Unterpunkten beschrieben.
           * F1-1-1 Erkennung von Szenenwechsel
           * Erkennung von  Szenenwechsel merkbar an das Videos. Ein Szenenwechsel tritt auf wenn sich das Motiv der Aufnahme ändert.
           * F1-1-2 Erkennung von Farbraumänderungen
           * Erkennung von Farbbereichen im Video.
           * F1-1-3 Erkennung von Helligkeitsschwankungen
           * Erkennung von Helligkeitsschwankungen, sowie Ab- und Aufblenden des Videos.
           * F1-1-4 Erkennung von Bewegung im Video
           * Erkennung von Kamerabewegungen im Video.
           * F1-1-5 Erkennung von individueller Bewegung
           * Bewegungen von Akteueren durch das Bild werden erkannt. Noch näher zu spezifizieren (Richtung, Geschwindigkeit)
           * F1-1-6 Erkennung von individuellen Elementen im Video
           * Elemente in Szenen, wie spezielle Akteure oder die Umgebung, in der sie sich bewegen, werden erkannt.
       * **F1-20 Musik-Erzeugung**
       * Die Musik wird dynamisch auf Basis der Video-Analyse(F1-1), welche zuvor geschieht, generiert.
           * **ToDo Beschreibungen**
           * F1-20-10 Erzeugung des Rhythmus, Beats und Taktes
           * F1-20-20 Erzeugung einer Melodie
           * F1-20-30 Nutzung verschiedener Klänge in der Melodie
           * F1-20-40 Lautmalerei für Ereignisse
           * F1-20-50 Musikgenerierung passend zu Anfang und Ende des Videos
           * Die Musik hat ein erkennbaren Start und Ende. Diese passen zeitlich zu Beginn und Ende des Videos.
           * F1-20-60 Lizenzfreie Musik
           * Die Musik, mit dem ein Video vertont wird, wird von der Anwendung dynamisch generiert. Es werden keine uhrheberrechtlich geschützten Musikstücke verwendet. Somit ist das Video im Bezug auf dessen Audio lizenzfrei verteilbar.
           * F1-20-70 Einzigartige Musik
           * Die Musik, mit dem ein Video vertont wird, wird von der Anwendung dynamisch generiert. Jede generierte Vertonung unterscheidet sich somit von anderen generierten Vertonungen sowie anderen vorher existenten Musikstücken.
           * F1-20-80 Anpassung der Musik auf die Ergebnisse der Analyse
           * **ToDo Beschreibung**
       * **F2 INTERFACE**
       * Um intuitive Nutzung zu garantieren wird eine graphische Oberfläche angeboten.  Um Videos schnell und einfach vertonen zu können, nutzt der Anwender die Benutzeroberfläche
           * F2-10 Import verschiedener Videos > noch umordnen, als Matrix?
           * Das Interface muss den Import von verschiedenen Videocontainern und Codecs unterstützen. Zu den Video-Containern gehören: MP4,AVI,MKV. Zu dem Video-Codecs gehören: MPEG-2, MPEG-4, h264, h265
           * **ToDo ID, Name**
           * MP4
           * **ToDo ID, Name**
           * AVI
           * **ToDo ID, Name**
           * MKV
           * F2-20 Socal-Media Anbindung
           * Die Videos können auf Youtube hochgeladen und anschließend geteilt werden.
           * F2-30 Speichern
           * Das Video wird in dem angegeben Speicherort gesichert.
           * F2-40 Desktopanwendung
           * Die Anwendung wird als plattformübergreifende Anwendung für den Desktop entwickelt. Der Nutzer kann sie als normale Anwendung installieren.
           * F2-50 Sponsorship von Titus erkennbar
           * In der Anwendung soll sichbar werden, dass Titus die Softwarelösung zur Verfügung stellt. Es gibt keine Zwangseinblendung im Video.
               * F2-50-10 Titus-Logo in der Anwendung
               * In der Anwendung soll das Logo von Titus immer sichbar sein.
               * F2-50-20 About-Kontakt
               * In der Anwendung finden sich die Kontakt-Daten von Titus.
       * **F3 EINSTELLUNGSMÖGLICHKEITEN FÜR DEN ANWENDER**
       * Der Anwender will sich mit dem Video identifizieren können. Er soll Einfluss auf die Generierung der Vertronung nehmen können um individuelle Präferenzen auszurdücken. Um die Anwendung einfach benutzbar zu halten werden nicht viele Einstellungsmöglichkeiten gegeben.
       * F3-10 Anpassung der Audio-Optionen
       * **ToDo Beschreibung**
       * F3-10-10 Musikgenerierungs-Stil
       * Damit der Anwender seinem Video eine individuellere Note geben kann, kann er aus verschiedenen Musikgenerierungsprofilen auswählen, die auf verschiedenen Musik-Bibliotheken und Effekten basieren.
       * F3-10-20 Geräusche aus Ursprungs-Video
       * Der Anwender kann festlegen, ob die Ursprungs-Geräusche aus dem Video bestehen bleiben oder nicht.
       * F3-20 Sprach-Einstellung
       * Deutsch und Englisch##***Nicht-Funktional***

##Nicht Funktional
   * NF1 Leichte Verteilungsmöglichkeit
   * Das Programm kann als eine Datei verteilt werden
   * NF2 Leichte Installation
   * Das Programm kann einfach gestartet werden oder ist leicht zu installieren.
   * NF3 Die Oberfläche hat ein modernes Interface
   * Die Oberfläche entspricht der heutigen Vorstellung eines schönen Designs.
   * NF4 Einfache Bedienung
   * Für die Erstellung des Videos sollen nicht mehr als vier Schritten notwendig sein.
   * NF5 Anwendung ist performant
   * Die Anwendung reagiert ohne Verzögerung auf die Interaktion der Nutzer.
       * NF5-1 kurze Reaktionszeit der Anwendung
       * Die Anwendung reagiert in unter einer Sekunde auf Aktionen des Anwenders.
       * NF5-2 Fortschrittsanzeige für längere Prozesse der Anwendung
       * Beim Prozess des Vertonens wird ein Fortschrittsbalken angezeigt.
   * NF6 Begrenzung der Dateigröße
   * Das Programm verhindert Fehler durch die Bearbeitung zu großer Dateien, indem es sie ablehnt.
   * NF7 Vermeidung von zu häufigem Musikwechsel
   * Offensichtlich unnötige oder unbegründete Wechsel der Musik werden vermieden.##

##Qualitäts und Leistungsmerkmale
###Funktionalität
  - Angemessenheit
  - Richtigkeit
  - Interoperabilität
  - Sicherheit
    Die Software ist für jeden installierbar, die Art der Verteilung liegt beim Kunden.

###Zuverlässigkeit
  - Reife
  - Fehlerfreiheit
  - Wiederherrstellbarkeit
    Bei einem Absturz werden die letzten Einstellungen beibehalten.
    Die temporären Daten werden nach jedem Absturz gelöscht.

###Robustheit
    Die Software fängt alle wahrscheinlichen Fehler ab und zeigt
    aussagekräftige Fehlermeldungen

###Benutzbarkeit
  - Verständlichkeit
    Die Software ist durch die Symbole und einfache Menüführung intuitiv zu bedienen.
  - Erlernbarkeit
    Es ist sind keine grundsätzlichen Vor- oder Fachkenntnisse für die Bedienung der Software notwendig.
  - Bedienbarkeit
    Es sind nicht mehr als 4 Schritte notwendig um die Software zu bedienen.

###Effizienz
  - Zeitverhalten
    Die Software benötigt auf aktuellen Rechnern nicht länger als die dreifache Länge der Videos.
  - Verbrauchsverhalten
    Die Software ist darauf programmiert alle ihr zur Verfügung stehenden Ressourcen zu nutzen.

###Änderbarkeit
  - Analysierbarkeit
  - Modifizierbarkeit
    In der Software lassen sich grundlegende Einstellungen vornehmen. Um den
    Nutzer nicht zu überfordern, wird alles weitere von der Software übernommen.
  - Stabilität
    Die Software soll keine fehlerhaften Eingaben zulassen und
    sich ohne Ressourcenmangel nicht unerwartet beenden.
  - Testbarkeit (JUnit)

###Technologieumfeld,Kompatibilität,Standards
  Die Software unterstützt die alle Desktop-Betriebssysteme mit einem Marktanteil von mehr als 5 %.
  Dazu gehören Windows 10, Windows 8/8.1, Windows 7 und MacOSX.

###Übertragbarkeit
  - Anpassbarkeit (?Sprache)
  - Installierbar
    Die Andwendung sollte einfach über einen Installer installier- oder deistallierbar sein.
  - Konformität
  - Austauschbar
